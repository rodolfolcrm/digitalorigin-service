package com.digitalorigin.digitaloriginservice.controller;


import com.digitalorigin.digitaloriginservice.DigitalOriginServiceApplication;
import com.digitalorigin.digitaloriginservice.dto.TransferAccountDTO;
import com.digitalorigin.digitaloriginservice.dto.TransferOrderDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = DigitalOriginServiceApplication.class
)
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class TransferControllerTest {

    @Autowired
    private MockMvc mockMvc;

    //both are on the same bank
    private TransferAccountDTO messiAccount = new TransferAccountDTO(1L, "Y1010101P", 1010);
    private TransferAccountDTO iniestaAccount = new TransferAccountDTO(1L, "Y8888888P", 8888);

    //different bank
    private TransferAccountDTO neymarAccount = new TransferAccountDTO(2L, "Y1111111P", 1111);


    @Test
    public void testTransferSameBank() throws Exception {
        test(messiAccount, iniestaAccount, new BigDecimal(10000), HttpStatus.OK);

    }

    @Test
    public void testTransferDifferentBankAmountUnder1000() throws Exception {
        test(messiAccount, neymarAccount, new BigDecimal(1000), HttpStatus.OK);
    }

    @Test
    public void testTransferDifferentBankAmountBigger1000() throws Exception {
        test(messiAccount, neymarAccount, new BigDecimal(1001), HttpStatus.BAD_REQUEST);
    }

    private void test(TransferAccountDTO fromAccount, TransferAccountDTO toAccount, BigDecimal ammount, HttpStatus expected) throws Exception {
        TransferOrderDTO transferOrderDTO = new TransferOrderDTO();
        transferOrderDTO.setAmount(ammount);
        transferOrderDTO.setTransferFromAccount(fromAccount);
        transferOrderDTO.setTransferToAccount(toAccount);

        String inputJson = mapToJson(transferOrderDTO);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/transfer")
                .accept(MediaType.APPLICATION_JSON)
                .content(inputJson)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals(expected.value(), response.getStatus());

    }


    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }
}
