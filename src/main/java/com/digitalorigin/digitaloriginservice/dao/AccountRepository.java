package com.digitalorigin.digitaloriginservice.dao;

import com.digitalorigin.digitaloriginservice.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Long> {

    Account findByAccountNumberAndCustomer_CustomerNumberAndCustomer_Bank_Id(Integer accountNumber, String customerNumber, Long bankId);
}
