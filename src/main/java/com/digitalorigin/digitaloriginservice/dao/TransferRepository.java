package com.digitalorigin.digitaloriginservice.dao;

import com.digitalorigin.digitaloriginservice.model.Transfer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransferRepository extends JpaRepository<Transfer, Long>{
}
