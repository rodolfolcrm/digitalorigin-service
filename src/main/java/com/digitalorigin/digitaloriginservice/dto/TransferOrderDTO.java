package com.digitalorigin.digitaloriginservice.dto;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class TransferOrderDTO {

    @NotNull(message = "transferFromAccount can not be null!")
    private TransferAccountDTO transferFromAccount;

    @NotNull(message = "transferToAccount can not be null!")
    private TransferAccountDTO transferToAccount;

    @NotNull(message = "amount can not be null!")
    private BigDecimal amount;


    public TransferAccountDTO getTransferFromAccount() {
        return transferFromAccount;
    }

    public void setTransferFromAccount(TransferAccountDTO transferFromAccount) {
        this.transferFromAccount = transferFromAccount;
    }

    public TransferAccountDTO getTransferToAccount() {
        return transferToAccount;
    }

    public void setTransferToAccount(TransferAccountDTO transferToAccount) {
        this.transferToAccount = transferToAccount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
