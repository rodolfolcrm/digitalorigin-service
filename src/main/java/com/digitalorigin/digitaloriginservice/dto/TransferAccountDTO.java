package com.digitalorigin.digitaloriginservice.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class TransferAccountDTO {

    @NotNull(message = "bankId can not be null!")
    private Long bankId;

    @NotEmpty(message = "customerNumber can not be null!")
    private String customerNumber;

    @NotNull(message = "accountNumber can not be null!")
    private Integer accountNumber;


    public TransferAccountDTO() {
    }

    public TransferAccountDTO(@NotNull(message = "bankId can not be null!") Long bankId, @NotEmpty(message = "customerNumber can not be null!") String customerNumber, @NotNull(message = "accountNumber can not be null!") Integer accountNumber) {
        this.bankId = bankId;
        this.customerNumber = customerNumber;
        this.accountNumber = accountNumber;
    }

    public Long getBankId() {
        return bankId;
    }

    public void setBankId(Long bankId) {
        this.bankId = bankId;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public Integer getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Integer accountNumber) {
        this.accountNumber = accountNumber;
    }
}
