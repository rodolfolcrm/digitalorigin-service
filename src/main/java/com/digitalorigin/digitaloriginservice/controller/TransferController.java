package com.digitalorigin.digitaloriginservice.controller;


import com.digitalorigin.digitaloriginservice.dto.TransferOrderDTO;
import com.digitalorigin.digitaloriginservice.exception.AccountNotFoundException;
import com.digitalorigin.digitaloriginservice.exception.TransferOrderAmountLimitExceededException;
import com.digitalorigin.digitaloriginservice.model.Account;
import com.digitalorigin.digitaloriginservice.model.Transfer;
import com.digitalorigin.digitaloriginservice.model.TransferType;
import com.digitalorigin.digitaloriginservice.service.account.AccountService;
import com.digitalorigin.digitaloriginservice.service.transfer.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("v1/transfer")
public class TransferController {

    private TransferService transferService;

    private AccountService accountService;

    @Autowired
    public TransferController(TransferService transferService, AccountService accountService) {
        this.transferService = transferService;
        this.accountService = accountService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public Transfer transferOrder(@Valid @RequestBody TransferOrderDTO transferOrder) throws AccountNotFoundException, TransferOrderAmountLimitExceededException{
        //TODO create mapper class to convert from DTO to model.
        Account transferFromAccount = accountService.findAccountBy(transferOrder.getTransferFromAccount().getAccountNumber(),
                transferOrder.getTransferFromAccount().getCustomerNumber(),
                transferOrder.getTransferFromAccount().getBankId());

        Account transferToAccount = accountService.findAccountBy(transferOrder.getTransferToAccount().getAccountNumber(),
                transferOrder.getTransferToAccount().getCustomerNumber(),
                transferOrder.getTransferToAccount().getBankId());

        TransferType transferType = transferFromAccount.getCustomer().getBank().getId() == transferToAccount.getCustomer().getBank().getId() ? TransferType.INTRA_BANK : TransferType.INTER_BANK;
        Transfer transfer = new Transfer();
        transfer.setFromAccount(transferFromAccount);
        transfer.setToAccount(transferToAccount);
        transfer.setTransferType(transferType);
        transfer.setAmount(transferOrder.getAmount());


        transferService.transfer(transfer);

        //TODO create DTO for model
        return transfer;
    }
}
