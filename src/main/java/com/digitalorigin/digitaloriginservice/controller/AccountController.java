package com.digitalorigin.digitaloriginservice.controller;


import com.digitalorigin.digitaloriginservice.model.Account;
import com.digitalorigin.digitaloriginservice.service.account.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/account")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @GetMapping
    public Page<Account> getCars(Pageable page) {
        //TODO create mapper class to convert from DTO to model.
        //TODO create DTO for model
        return accountService.findAll(page);
    }
}
