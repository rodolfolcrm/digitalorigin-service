package com.digitalorigin.digitaloriginservice.service.account;

import com.digitalorigin.digitaloriginservice.exception.AccountNotFoundException;
import com.digitalorigin.digitaloriginservice.model.Account;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface AccountService {
    Account findAccountBy(Integer accountNumber, String customerNumber, Long bankId) throws AccountNotFoundException;

    Page<Account> findAll(Pageable page);
}
