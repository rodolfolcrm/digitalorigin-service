package com.digitalorigin.digitaloriginservice.service.account;

import com.digitalorigin.digitaloriginservice.dao.AccountRepository;
import com.digitalorigin.digitaloriginservice.exception.AccountNotFoundException;
import com.digitalorigin.digitaloriginservice.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService{

    private AccountRepository accountRepository;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Account findAccountBy(Integer accountNumber, String customerNumber, Long bankId) throws AccountNotFoundException {
        Account account = accountRepository.findByAccountNumberAndCustomer_CustomerNumberAndCustomer_Bank_Id(accountNumber, customerNumber, bankId);
        if(account==null)
            throw new AccountNotFoundException();

        return account;
    }

    @Override
    public Page<Account> findAll(Pageable page) {
        return accountRepository.findAll(page);
    }
}
