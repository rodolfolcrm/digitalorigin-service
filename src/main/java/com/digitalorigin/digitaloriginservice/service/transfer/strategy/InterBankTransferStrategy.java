package com.digitalorigin.digitaloriginservice.service.transfer.strategy;

import com.digitalorigin.digitaloriginservice.exception.TransferOrderAmountLimitExceededException;
import com.digitalorigin.digitaloriginservice.model.Account;
import com.digitalorigin.digitaloriginservice.model.Transfer;
import com.digitalorigin.digitaloriginservice.model.TransferType;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class InterBankTransferStrategy extends  IntraBankTransferStrategy {

    public static final int LIMIT = 1000;
    public static final int COMMISSION = 5;

    @Override
    public void execute(Transfer transfer) throws TransferOrderAmountLimitExceededException{
        if(transfer.getAmount().compareTo(new BigDecimal(LIMIT))>0)
            throw new TransferOrderAmountLimitExceededException();

        super.execute(transfer);

        Account fromAccount = transfer.getFromAccount();
        fromAccount.setAmount(fromAccount.getAmount().subtract(new BigDecimal(COMMISSION)));
    }

    @Override
    public TransferType getType() {
        return TransferType.INTER_BANK;
    }
}
