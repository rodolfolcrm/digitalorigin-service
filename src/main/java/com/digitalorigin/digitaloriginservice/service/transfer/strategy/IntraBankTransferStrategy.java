package com.digitalorigin.digitaloriginservice.service.transfer.strategy;

import com.digitalorigin.digitaloriginservice.exception.TransferOrderAmountLimitExceededException;
import com.digitalorigin.digitaloriginservice.model.Account;
import com.digitalorigin.digitaloriginservice.model.Transfer;
import com.digitalorigin.digitaloriginservice.model.TransferType;
import org.springframework.stereotype.Component;

@Component
public class IntraBankTransferStrategy implements TransferStrategy {
    @Override
    public void execute(Transfer transfer) throws TransferOrderAmountLimitExceededException {
        Account fromAccount = transfer.getFromAccount();
        Account toAccount = transfer.getToAccount();
        fromAccount.setAmount(fromAccount.getAmount().subtract(transfer.getAmount()));
        toAccount.setAmount(toAccount.getAmount().add(transfer.getAmount()));
    }

    @Override
    public TransferType getType() {
        return TransferType.INTRA_BANK;
    }
}
