package com.digitalorigin.digitaloriginservice.service.transfer;

import com.digitalorigin.digitaloriginservice.exception.TransferOrderAmountLimitExceededException;
import com.digitalorigin.digitaloriginservice.model.Transfer;

import javax.validation.Valid;

public interface TransferService {
    Transfer transfer(@Valid Transfer transfer)throws TransferOrderAmountLimitExceededException;
}
