package com.digitalorigin.digitaloriginservice.service.transfer;

import com.digitalorigin.digitaloriginservice.dao.AccountRepository;
import com.digitalorigin.digitaloriginservice.dao.TransferRepository;
import com.digitalorigin.digitaloriginservice.exception.TransferOrderAmountLimitExceededException;
import com.digitalorigin.digitaloriginservice.model.Transfer;
import com.digitalorigin.digitaloriginservice.service.transfer.strategy.TransferStrategyFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;

@Service
public class TransferServiceImpl implements TransferService {

    @Autowired
    private TransferRepository transferRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private TransferStrategyFactory transferStrategyFactory;

    @Override
    public Transfer transfer(@Valid Transfer transfer) throws TransferOrderAmountLimitExceededException {
        transferStrategyFactory.getTransferStrategy(transfer.getTransferType()).execute(transfer);
        transferRepository.save(transfer);
        accountRepository.save(transfer.getFromAccount());
        accountRepository.save(transfer.getToAccount());
        return transfer;
    }
}
