package com.digitalorigin.digitaloriginservice.service.transfer.strategy;

import com.digitalorigin.digitaloriginservice.model.TransferType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class TransferStrategyFactory {

    @Autowired
    private List<TransferStrategy> transferStrategyList;

    private Map<TransferType, TransferStrategy> transferStrategyCache = new HashMap<>();

    @PostConstruct
    public void initCache(){
        for (TransferStrategy ts : transferStrategyList) {
            transferStrategyCache.put(ts.getType(), ts);
        }
    }

    public TransferStrategy getTransferStrategy(TransferType transferType){
        TransferStrategy transferStrategy = transferStrategyCache.get(transferType);
        if(transferStrategy == null) throw new RuntimeException("There is not TransferStrategy implemented for: " + transferType);
        return transferStrategy;
    }
}
