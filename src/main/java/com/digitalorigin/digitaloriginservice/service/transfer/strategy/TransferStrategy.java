package com.digitalorigin.digitaloriginservice.service.transfer.strategy;

import com.digitalorigin.digitaloriginservice.exception.TransferOrderAmountLimitExceededException;
import com.digitalorigin.digitaloriginservice.model.Transfer;
import com.digitalorigin.digitaloriginservice.model.TransferType;


public interface TransferStrategy {

    void execute(Transfer transfer) throws TransferOrderAmountLimitExceededException;

    TransferType getType();
}
