package com.digitalorigin.digitaloriginservice.model;

public enum TransferType {
    INTRA_BANK,
    INTER_BANK
}
