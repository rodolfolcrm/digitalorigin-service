package com.digitalorigin.digitaloriginservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Table
@Entity
public class Transfer {
    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;

    @Column(nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonIgnore
    private ZonedDateTime dateCreated = ZonedDateTime.now();

    @ManyToOne
    @JoinColumn(name = "from_account_id")
    @NotNull(message = "fromAccount can not be null!")
    private Account fromAccount;

    @ManyToOne
    @JoinColumn(name = "to_account_id")
    @NotNull(message = "toAccount can not be null!")
    private Account toAccount;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    @NotNull(message = "Transfer Type can not be null!")
    @JsonIgnore
    private TransferType transferType;

    @Column
    @NotNull(message = "Amount can not be null!")
    private BigDecimal amount;


    public Transfer() {
    }

    public Transfer(ZonedDateTime dateCreated, Account fromAccount, Account toAccount, TransferType transferType, BigDecimal amount) {
        this.dateCreated = dateCreated;
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
        this.transferType = transferType;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Account getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(Account fromAccount) {
        this.fromAccount = fromAccount;
    }

    public Account getToAccount() {
        return toAccount;
    }

    public void setToAccount(Account toAccount) {
        this.toAccount = toAccount;
    }

    public TransferType getTransferType() {
        return transferType;
    }

    public void setTransferType(TransferType transferType) {
        this.transferType = transferType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
