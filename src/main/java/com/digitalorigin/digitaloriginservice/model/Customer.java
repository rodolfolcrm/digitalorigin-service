package com.digitalorigin.digitaloriginservice.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(
        uniqueConstraints = @UniqueConstraint(name = "uc_customer", columnNames = {"customerNumber", "bank_id"})//assuming that same customerNumber can have different banks.
)
public class Customer {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    @NotNull(message = "customerNumber can not be null!")
    private String customerNumber;

    @Column(nullable = false)
    @NotNull(message = "Name can not be null!")
    private String name;

    @ManyToOne
    @JoinColumn(name = "bank_id")
    @NotNull(message = "bank can not be null!")
    private Bank bank;

    @JsonIgnore
    @OneToMany(mappedBy = "customer")
    private List<Account> accounts;

    public Customer() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }
}
