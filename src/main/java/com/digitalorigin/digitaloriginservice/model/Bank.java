package com.digitalorigin.digitaloriginservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table
public class Bank {

    @Id
    private Long id;

    @Column(nullable = false)
    @NotNull(message = "Name can not be null!")
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "bank")
    private List<Customer> customers;

    public Bank() {
    }

    public Bank(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
