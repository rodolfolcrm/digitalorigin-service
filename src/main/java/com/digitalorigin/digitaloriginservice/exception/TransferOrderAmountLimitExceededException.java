package com.digitalorigin.digitaloriginservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Ammount for tranfer exceeded")
public class TransferOrderAmountLimitExceededException extends Exception{

    public TransferOrderAmountLimitExceededException()
    {
        super();
    }

    public TransferOrderAmountLimitExceededException(String message)
    {
        super(message);
    }
}
