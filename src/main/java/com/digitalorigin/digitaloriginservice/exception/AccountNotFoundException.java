package com.digitalorigin.digitaloriginservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Account Not Found")
public class AccountNotFoundException extends Exception{

    public AccountNotFoundException()
    {
        super();
    }

    public AccountNotFoundException(String message)
    {
        super(message);
    }
}
