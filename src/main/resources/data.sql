--Create banks
insert into bank (id, name) values (1, 'Sabadell');
insert into bank (id, name) values (2, 'La Caixa');
insert into bank (id, name) values (3, 'Deutche Bank');

--Create customers
--same bank
insert into customer (id, name, customer_number, bank_id) values (10,'Messi', 'Y1010101P', 1);
insert into customer (id, name, customer_number, bank_id) values (8,'Iniesta', 'Y8888888P', 1);

insert into customer (id, name, customer_number, bank_id) values (11,'Neymar', 'Y1111111P', 2);
insert into customer (id, name, customer_number, bank_id) values (1,'Ter Stegen', 'Y0101010P', 3);


--Create Accounts
insert into account (id, account_number, amount, customer_id) values (1, 1010, 9000000, 10);
insert into account (id, account_number, amount, customer_id) values (2, 8888, 5000000, 8);
insert into account (id, account_number, amount, customer_id) values (3, 1111, 7000000, 11);
insert into account (id, account_number, amount, customer_id) values (4, 0101, 3000000, 1);
